package com.moneytracker.dto

import java.util.Currency

case class Money(amount: BigDecimal, currency: Currency)
