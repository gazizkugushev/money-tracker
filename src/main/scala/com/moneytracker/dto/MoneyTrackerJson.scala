package com.moneytracker.dto

import java.time.format.DateTimeFormatter
import java.time.{LocalDate, LocalDateTime}
import java.util.Currency

import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

class JsStringFormat[T](fromString: String => T, toString: T => String,
                        val fieldName: String, correctExample: String) extends RootJsonFormat[T] {
  override def read(json: JsValue): T = json match {
    case JsString(string) =>
      try {
        fromString(string)
      } catch {
        case e: Exception => throwParsingException(json, Some(e))
      }
    case _ => throwParsingException(json, None)
  }

  protected def throwParsingException(json: JsValue, cause: Option[Throwable]): Nothing = {
    val msg = s"Can't parse $fieldName=$json. Example: $correctExample"
    cause match {
      case Some(exception) => throw new IllegalArgumentException(msg, exception)
      case None => throw new IllegalArgumentException(msg)
    }
  }

  override def write(obj: T): JsValue = JsString(toString(obj))
}

object MoneyTrackerJson extends DefaultJsonProtocol {
  implicit val currencyFormat = new JsStringFormat[Currency](Currency.getInstance, _.getCurrencyCode, "currency", "USD")
  implicit val dateTimeFormat = new JsStringFormat[LocalDateTime](
    jsonStr => LocalDateTime.parse(jsonStr, DateTimeFormatter.ISO_DATE_TIME),
    DateTimeFormatter.ISO_DATE_TIME.format,
    "date",
    "2016-03-25T11:25:56.000000012"
  )
  implicit val dateFormat = new JsStringFormat[LocalDate](
    jsonStr => LocalDate.parse(jsonStr, DateTimeFormatter.ISO_DATE),
    DateTimeFormatter.ISO_DATE.format,
    "date",
    "2016-03-25"
  )
  implicit val transactionFormat = jsonFormat4(Transaction)
  implicit val balanceFormat = jsonFormat2(Money)
  implicit val userFormat = jsonFormat2(User)
}
