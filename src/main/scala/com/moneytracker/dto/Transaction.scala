package com.moneytracker.dto

import java.time.LocalDateTime
import java.util.Currency

case class Transaction(amount: BigDecimal, currency: Currency, date: LocalDateTime, message: Option[String]) {
}
