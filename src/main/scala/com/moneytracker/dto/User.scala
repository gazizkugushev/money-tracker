package com.moneytracker.dto

case class User(email: String, password: Option[String])
