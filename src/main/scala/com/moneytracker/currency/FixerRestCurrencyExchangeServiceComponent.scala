package com.moneytracker.currency

import java.time.LocalDate
import java.util.Currency
import java.util.concurrent.ConcurrentHashMap

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source
import com.moneytracker.dto.MoneyTrackerJson._
import spray.client.pipelining._
import spray.http._
import spray.httpx.SprayJsonSupport._
import spray.json.DefaultJsonProtocol

import scala.collection.convert.decorateAsScala._
import scala.concurrent.Future

trait FixerRestCurrencyExchangeServiceComponent extends CurrencyExchangeServiceComponent {

  object FixerJsonProtocol extends DefaultJsonProtocol {
    implicit val fixerResponseFormat = jsonFormat3(FixerResponse)
  }

  import FixerJsonProtocol._

  implicit def actorSystem: ActorSystem

  implicit def dispatcher = actorSystem.dispatcher

  override def currencyExchangeService: CurrencyExchangeService = new FixerRestCurrencyExchangeService(
    sendReceive ~> unmarshal[FixerResponse]
  )

  class FixerRestCurrencyExchangeService(private val pipeline: HttpRequest => Future[FixerResponse])
    extends CurrencyExchangeService {

    private val cache = new ConcurrentHashMap[CurrencyCodesExchangePair, BigDecimal]().asScala

    override def exchangeRateByCurrencyCodes(from: Seq[Currency], to: Currency): Source[Map[String, BigDecimal], NotUsed] = {
      val toCode = to.getCurrencyCode
      val (fromCodes, fromCodesWithToCode): (Set[String], Set[String]) = from.map(_.getCurrencyCode)
        .toSet
        .partition(_ != toCode)
      val (cachedEntries, codesAmountsToQuery) = (for (fromCode <- fromCodes)
        yield (fromCode, cache.get(CurrencyCodesExchangePair(fromCode, toCode))))
        .partition(_._2.isDefined)
      val resultsFromCache: Map[String, BigDecimal] = cachedEntries.map(pair => (pair._1, pair._2.get)).toMap
      val resultsToItself: Map[String, BigDecimal] = if (fromCodesWithToCode.isEmpty) Map() else Map(toCode -> 1)
      if (codesAmountsToQuery.isEmpty) {
        Source.single(resultsFromCache ++ resultsToItself)
      } else {
        val codesToQuery: Set[String] = codesAmountsToQuery.map(_._1)
        Source.fromFuture {
          val symbolsParam = codesToQuery.mkString(",")
          pipeline(Get(s"http://api.fixer.io/latest?base=$toCode&symbols=$symbolsParam"))
        }.map { fixerResponse =>
          val responseRates: Map[String, BigDecimal] = fixerResponse.rates
          val codesWithNoReturns = codesToQuery -- responseRates.keySet
          if (codesWithNoReturns.nonEmpty) throw new IllegalArgumentException(s"Can't load $codesWithNoReturns from Fixer")
          responseRates.foreach { case (code, rate) => cache.put(CurrencyCodesExchangePair(code, toCode), rate) }
          responseRates ++ resultsFromCache ++ resultsToItself
        }
      }
    }

    private case class CurrencyCodesExchangePair(from: String, to: String)

  }


}

case class FixerResponse(base: Currency, date: LocalDate, rates: Map[String, BigDecimal])
