package com.moneytracker.currency

import java.util.Currency

import akka.NotUsed
import akka.stream.scaladsl.Source

trait CurrencyExchangeServiceComponent {

  def currencyExchangeService: CurrencyExchangeService

  trait CurrencyExchangeService {
    def exchangeRateByCurrencyCodes(from: Seq[Currency], to: Currency): Source[Map[String, BigDecimal], NotUsed]
  }

}
