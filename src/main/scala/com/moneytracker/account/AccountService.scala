package com.moneytracker.account

import java.time.LocalDate
import java.util.Currency

import akka.actor.{Actor, ActorSystem}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}
import com.moneytracker.currency.{CurrencyExchangeServiceComponent, FixerRestCurrencyExchangeServiceComponent}
import com.moneytracker.dto.MoneyTrackerJson._
import com.moneytracker.dto.{Money, Transaction}
import com.typesafe.config.{Config, ConfigFactory}
import shapeless.{::, HNil}
import spray.http.MediaTypes._
import spray.http.StatusCodes
import spray.httpx.SprayJsonSupport.{sprayJsonMarshaller, sprayJsonUnmarshaller}
import spray.json.JsString
import spray.routing.authentication.{BasicAuth, UserPass}
import spray.routing.{Directive, ExceptionHandler, HttpService}
import spray.util.LoggingContext

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}


class AccountServiceActor extends Actor
  with AccountService with MongodbAccountsDaoComponent with FixerRestCurrencyExchangeServiceComponent {

  override val basicAuthConfig: Config = ConfigFactory.load.getConfig("accounts.user")

  override val actorRefFactory = context

  override def receive = runRoute(routes)

  override implicit def actorSystem: ActorSystem = context.system

  override implicit val materializer = ActorMaterializer()
}


trait AccountService extends HttpService {

  this: AccountsDaoComponent with CurrencyExchangeServiceComponent =>

  implicit def materializer: ActorMaterializer

  val basicAuthConfig: Config

  val startDateEndDateParameters: Directive[::[(LocalDate, LocalDate), HNil]] = parameters('startDate, 'endDate).hmap {
    case startDate :: endDate :: HNil => (dateFormat.read(JsString(startDate)), dateFormat.read(JsString(endDate)))
  }

  val dateCurrencyParameters: Directive[::[(LocalDate, Currency), HNil]] = parameters('date, 'currency).hmap {
    case date :: currency :: HNil => (dateFormat.read(JsString(date)), currencyFormat.read(JsString(currency)))
  }

  implicit def myExceptionHandler(implicit log: LoggingContext) = ExceptionHandler {
    case e: IllegalArgumentException =>
      requestUri { uri =>
        log.error(e, "Request to {} could not be handled normally, exception={} {}", uri)
        complete(StatusCodes.BadRequest, e.getMessage)
      }
    case e =>
      requestUri { uri =>
        log.error(e, "Request to {} could not be handled normally", uri)
        complete(StatusCodes.InternalServerError, e.getMessage)
      }
  }

  def extractUser(userPass: UserPass): String = userPass.user

  lazy val routes = {
    respondWithMediaType(`application/json`) {
      import spray.routing.PathMatcher._

      import ExecutionContext.Implicits.global

      pathPrefix("accounts") {
        authenticate(BasicAuth(realm = "Money tracker accounts service", basicAuthConfig, extractUser _)) { userName =>
          path("transactions") {
            post {
              entity(as[Transaction]) { transaction =>
                val result: Future[Done] = accountsDao.insertTransaction(transaction).runWith(Sink.head)
                onComplete(result) {
                  case Success(Done(msg)) => complete(StatusCodes.Created)
                  case Failure(ex) => complete(StatusCodes.InternalServerError)
                }
              }
            } ~ get {
              startDateEndDateParameters { case (startDate, endDate) =>
                val result: Future[Seq[Transaction]] = accountsDao.transactionsWithin(startDate, endDate)
                  .grouped(Int.MaxValue)
                  // In case if stream is DAO stream is empty we need stream of empty list
                  // to be transformed into future. This source will never be invoked in case
                  // if original DAO stream is not empty
                  .concat(Source.single(List()))
                  .runWith(Sink.head)
                onComplete(result) {
                  case Success(transactions) => complete(transactions)
                  case Failure(ex) => complete(StatusCodes.InternalServerError)
                }
              }
            }
          } ~ path("balance") {
            get {
              dateCurrencyParameters { case (date, currency) =>
                val startDate = LocalDate.ofEpochDay(0)
                val endDate = date.plusDays(1)
                val balanceFuture: Future[BigDecimal] = accountsDao.transactionsWithin(startDate, endDate)
                  .map(txn => (txn.amount, txn.currency))
                  .grouped(Int.MaxValue)
                  .flatMapConcat(amountCurrencyPairs => {
                    val (_, currencies) = amountCurrencyPairs.unzip
                    currencyExchangeService.exchangeRateByCurrencyCodes(currencies, currency)
                      .zip(Source.single(amountCurrencyPairs))
                  })
                  .map { case (exchangeRates, amountCurrencyPairs) =>
                    amountCurrencyPairs.foldLeft(BigDecimal(0)) { case (acc, (amount, amountCurrency)) =>
                      val exchangeRate: BigDecimal = exchangeRates(amountCurrency.getCurrencyCode)
                      acc + (amount / exchangeRate)
                    }
                  }
                  .concat(Source.single(BigDecimal(0))) // If there are no transactions we should return 0
                  .runWith(Sink.head)
                onComplete(balanceFuture) {
                  case Success(balance) => complete(Money(balance, currency))
                  case Failure(ex) => complete(StatusCodes.InternalServerError)
                }
              }
            }
          }
        }
      }
    }
  }
}
