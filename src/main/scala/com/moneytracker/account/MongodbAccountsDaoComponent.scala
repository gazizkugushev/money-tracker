package com.moneytracker.account

import java.time._
import java.util.Currency

import akka.NotUsed
import akka.actor.ActorSystem
import akka.event.Logging
import akka.stream.scaladsl.Source
import com.moneytracker.MongoImplicits
import com.moneytracker.dto.Transaction
import com.typesafe.config.ConfigFactory
import org.mongodb.scala.bson.BsonDateTime
import org.mongodb.scala.model.Filters.{and, gte, lt}
import org.mongodb.scala.model.Indexes
import org.mongodb.scala.{Document, MongoClient, MongoCollection}

import scala.language.implicitConversions

trait MongodbAccountsDaoComponent extends AccountsDaoComponent {

  implicit def actorSystem: ActorSystem

  private val log = Logging(actorSystem, this.getClass)
  override val accountsDao: MongodbAccountsDao = new MongodbAccountsDao


  class MongodbAccountsDao extends AccountsDao {

    import MongodbAccountsDao._


    val config = ConfigFactory.load
    private val mongoClient: MongoClient = MongoClient(config.getString("accounts.mongo.url"))
    private val db = mongoClient.getDatabase("account-service")
    private val transactions = {
      val collection: MongoCollection[Document] = db.getCollection("transactions")
      collection.createIndex(Indexes.ascending(Fields.Date)).subscribe(
        (result: String) => log.info("date index was created with result '{}'", result),
        (exception: Throwable) => log.error(exception, "Error while creating date index")
      )
      collection
    }

    override def insertTransaction(transaction: Transaction): Source[Done, NotUsed] = {
      Source.single(transaction)
        .map {
          txn =>
            val msgDocument = txn.message.map(msg => Document(Fields.Message -> msg)).getOrElse(Document.empty)
            msgDocument ++ Document(
            "amount" -> txn.amount.toString(),
            "currency" -> txn.currency.getCurrencyCode,
              "date" -> dateTime2Bson(txn.date)
          )
        }
        .flatMapConcat(document => Source.fromFuture(transactions.insertOne(document).head()))
        .map(completed => Done("Credit transaction saved"))
    }

    override def transactionsWithin(startDate: LocalDate, endDate: LocalDate): Source[Transaction, NotUsed] =
      Source
        .fromPublisher {
          val startDateTime = dateTime2Bson(startDate.atStartOfDay())
          val endDateTime = dateTime2Bson(endDate.atStartOfDay())
          MongoImplicits.observable2Publisher(transactions.find(and(gte(Fields.Date, startDateTime), lt(Fields.Date, endDateTime))))
        }.map(doc => Transaction(
        BigDecimal(doc(Fields.Amount).asString().getValue),
        Currency.getInstance(doc(Fields.Currency).asString().getValue),
        doc(Fields.Date).asDateTime(),
        doc.get(Fields.Message).map(_.asString().getValue)
      )
      )

    def close() = mongoClient.close()
  }

  private object Fields {
    val Amount = "amount"
    val Currency = "currency"
    val Date = "date"
    val Message = "message"
  }

  object MongodbAccountsDao {
    val DefaultZoneOffset = ZoneOffset.UTC

    implicit def dateTime2Bson(localDateTime: LocalDateTime): BsonDateTime =
      BsonDateTime(localDateTime.atZone(DefaultZoneOffset).toInstant.toEpochMilli)

    implicit def bson2dateTime(bsonDateTime: BsonDateTime): LocalDateTime =
      Instant.ofEpochMilli(bsonDateTime.getValue).atZone(DefaultZoneOffset).toLocalDateTime
  }

}

