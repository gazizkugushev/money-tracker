package com.moneytracker.account

import java.time.LocalDate

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.moneytracker.dto.Transaction

trait AccountsDaoComponent {

  def accountsDao: AccountsDao

  trait AccountsDao {
    def insertTransaction(transaction: Transaction): Source[Done, NotUsed]

    def transactionsWithin(startDate: LocalDate, endDate: LocalDate): Source[Transaction, NotUsed]
  }

  case class Done(message: String)
}



