package com.moneytracker

import org.mongodb.scala.{Observable => MdbObservable, Observer => MdbObserver}
import org.reactivestreams.{Publisher, Subscriber, Subscription}

import scala.language.implicitConversions

object MongoImplicits {

  implicit def observable2Publisher[T](mdbObservable: MdbObservable[T]): Publisher[T] = new Publisher[T] {
    override def subscribe(s: Subscriber[_ >: T]): Unit = {
      mdbObservable.subscribe {
        val mdbObserver = new MdbObserver[T] {

          override def onError(e: Throwable): Unit = s.onError(e)

          override def onComplete(): Unit = s.onComplete()

          override def onNext(result: T): Unit = s.onNext(result)
        }
        s.onSubscribe(new Subscription {
          override def cancel(): Unit = mdbObserver.onComplete()

          override def request(n: Long): Unit = {}
        })
        mdbObserver
      }
    }
  }
}
