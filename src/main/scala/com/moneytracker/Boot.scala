package com.moneytracker

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.moneytracker.account.AccountServiceActor
import com.typesafe.config.ConfigFactory
import spray.can.Http

import scala.concurrent.duration._

object Boot extends App {

  val config = ConfigFactory.load.getConfig("accounts.host")
  val accountsSystem = ActorSystem("accounts")

  val accountsService = accountsSystem.actorOf(Props[AccountServiceActor], "accounts-service")

  implicit val timeout = Timeout(5.seconds)
  IO(Http)(accountsSystem) ? Http.Bind(
    accountsService,
    interface = config.getString("interface"),
    port = config.getInt("port"))
}
