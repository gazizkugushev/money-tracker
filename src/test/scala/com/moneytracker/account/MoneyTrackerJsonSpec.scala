package com.moneytracker.account

import java.time.{LocalDate, LocalDateTime}
import java.util.Currency

import com.moneytracker.dto.MoneyTrackerJson
import com.moneytracker.specs.UnitSpec
import spray.json.{JsArray, JsString, RootJsonFormat}

class MoneyTrackerJsonSpec extends UnitSpec {

  def JsStringSpecs[T](uutName: String, uut: RootJsonFormat[T], value: T, correctJsValue: String, incorrectJsValue: String) = {
    describe(s"$uutName") {
      it("write should return proper JSON") {
        uut.write(value) shouldBe JsString(correctJsValue)
      }
      describe("read") {
        it("should return proper currency") {
          uut.read(JsString(correctJsValue)) shouldBe value
        }
        describe("should throw IllegalArgumentException") {
          it("when value is not JsString") {
            an[IllegalArgumentException] shouldBe thrownBy(MoneyTrackerJson.currencyFormat.read(JsArray()))
          }
          it("when value is invalid format") {
            an[IllegalArgumentException] shouldBe thrownBy(MoneyTrackerJson.currencyFormat.read(JsString(incorrectJsValue)))
          }
        }
      }
    }
  }

  JsStringSpecs(
    uutName = "AccountServiceJson.currencyFormat",
    uut = MoneyTrackerJson.currencyFormat,
    value = Currency.getInstance("USD"),
    correctJsValue = "USD",
    incorrectJsValue = "incorrect currency"
  )

  JsStringSpecs(
    uutName = "AccountServiceJson.dateTimeFormatter",
    uut = MoneyTrackerJson.dateTimeFormat,
    value = LocalDateTime.of(2016, 3, 25, 11, 25, 56, 12),
    correctJsValue = "2016-03-25T11:25:56.000000012",
    incorrectJsValue = "incorrect date time"
  )

  JsStringSpecs(
    uutName = "AccountServiceJson.dateFormatter",
    uut = MoneyTrackerJson.dateFormat,
    value = LocalDate.of(2016, 3, 25),
    correctJsValue = "2016-03-25",
    incorrectJsValue = "incorrect date time"
  )

}
