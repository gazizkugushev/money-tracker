package com.moneytracker.account

import java.time.LocalDateTime
import java.util.Currency

import akka.actor.ActorRefFactory
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.moneytracker.currency.CurrencyExchangeServiceComponent
import com.moneytracker.dto.MoneyTrackerJson._
import com.moneytracker.dto.{Money, Transaction}
import com.moneytracker.specs.RestSpec
import com.typesafe.config.{Config, ConfigFactory}
import spray.http.{BasicHttpCredentials, HttpEntity, MediaTypes, StatusCodes}
import spray.httpx.SprayJsonSupport._

import scala.concurrent.Future

class AccountServiceSpec extends RestSpec with AccountService
  with AccountsDaoComponent with CurrencyExchangeServiceComponent {

  override def actorRefFactory: ActorRefFactory = system

  override implicit def materializer: ActorMaterializer = ActorMaterializer()

  override val basicAuthConfig: Config = ConfigFactory.parseString("user = password")
  override val accountsDao = mock[AccountsDao]
  override val currencyExchangeService = mock[CurrencyExchangeService]

  val txn1: Transaction = Transaction(
    42,
    Currency.getInstance("USD"),
    LocalDateTime.of(2016, 3, 23, 15, 33, 12, 25),
    Some("message")
  )
  val txn2: Transaction = Transaction(
    43,
    Currency.getInstance("EUR"),
    LocalDateTime.of(2016, 3, 24, 15, 33, 12, 25),
    Some("message")
  )
  val credentials = BasicHttpCredentials("user", "password")

  describe("AccountService") {
    describe("/accounts/balance GET should return") {
      val url = "/accounts/balance"

      describe("OK(200)") {
        it("with response body when transaction history exists") {
          (accountsDao.transactionsWithin _).expects(*, *).returns(Source(List(txn1, txn2)))
          val usd2gbpRate: BigDecimal = 2
          val eur2gpbRate: BigDecimal = 4
          (currencyExchangeService.exchangeRateByCurrencyCodes _).expects(*, *).returns(Source.single(Map(
            "USD" -> usd2gbpRate,
            "EUR" -> eur2gpbRate
          )))
          Get(s"$url?date=2011-12-03&currency=GBP") ~> addCredentials(credentials) ~> routes ~> check {
            responseAs[Money] shouldBe Money(txn1.amount / usd2gbpRate + txn2.amount / eur2gpbRate, Currency.getInstance("GBP"))
          }
        }
        it("with amount 0 when transaction history does not exist") {
          (accountsDao.transactionsWithin _).expects(*, *).returns(Source.empty)
          (currencyExchangeService.exchangeRateByCurrencyCodes _).expects(*, *).never()
          Get(s"$url?date=2011-12-03&currency=GBP") ~> addCredentials(credentials) ~> routes ~> check {
            responseAs[Money] shouldBe Money(0, Currency.getInstance("GBP"))
          }
        }
      }
      it("BadRequest(400) when date is not able to be parsed") {
        Get(s"$url?date=incorrectDate&currency=GBP") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.BadRequest
        }
      }
      it("NotFound(404) when date is absent") {
        Get(s"$url?currency=GBP") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.NotFound
        }
      }
      it("BadRequest(400) when currency is not able to be parsed") {
        Get(s"$url?date=2013-03-04&currency=illegalCurrency") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.BadRequest
        }
      }
      it("NotFound(404) when currency is absent") {
        Get(s"$url?date=2013-03-04") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.NotFound
        }
      }
      it("InternalServerError(500) when currency exchange server returns failed stream") {
        (accountsDao.transactionsWithin _).expects(*, *).returns(Source(List(txn1, txn2)))
        (currencyExchangeService.exchangeRateByCurrencyCodes _).expects(*, *).returns(Source.fromFuture(Future(
          throw new Exception("For test purpose")
        )))
        Get(s"$url?date=2011-12-03&currency=GBP") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.InternalServerError
        }
      }
      it("Unauthorized(401) when credentials are not provided") {
        Get(s"$url?date=2011-12-03&currency=GBP") ~> sealRoute(routes) ~> check {
          status shouldBe StatusCodes.Unauthorized
        }
      }
    }
    describe("/accounts/transactions") {
      val url = "/accounts/transactions"
      describe("GET should return") {
        it("OK(200) with list of transactions when they do exist") {
          (accountsDao.transactionsWithin _).expects(*, *).returns(Source(Vector(txn1, txn2)))
          Get(s"$url?startDate=2011-12-03&endDate=2011-12-05") ~> addCredentials(credentials) ~> routes ~> check {
            responseAs[List[Transaction]] shouldBe List(txn1, txn2)
          }
        }
        it("OK(200) with empty list when transactions don't exist") {
          (accountsDao.transactionsWithin _).expects(*, *).returns(Source(Vector[Transaction]()))
          Get(s"$url?startDate=2011-12-03&endDate=2011-12-05") ~> addCredentials(credentials) ~> routes ~> check {
            responseAs[List[Transaction]] shouldBe List()
          }
        }
        describe("BadRequest(400)") {
          it("when startDate is in wrong format") {
            Get(s"$url?startDate=wrongFormat&endDate=2011-12-05") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
              status shouldBe StatusCodes.BadRequest
            }
          }
          it("when endDate is in wrong format") {
            Get(s"$url?startDate=2011-12-05&endDate=wrongFormat") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
              status shouldBe StatusCodes.BadRequest
            }
          }
        }
        describe("NotFound(404)") {
          it("when startDate is absent") {
            Get(s"$url?endDate=2011-12-05") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
              status shouldBe StatusCodes.NotFound
            }
          }
          it("when endDate is absent") {
            Get(s"$url?startDate=2011-12-05") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
              status shouldBe StatusCodes.NotFound
            }
          }
        }
        it("InternalServerError(500) when transactionsWithin returns failed stream") {
          (accountsDao.transactionsWithin _).expects(*, *).returns(Source.fromFuture(Future {
            throw new Exception("For test purpose")
          }))
          Get(s"$url?startDate=2011-12-03&endDate=2011-12-05") ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
            status shouldBe StatusCodes.InternalServerError
          }
        }
        it("Unauthorized(401) when credentials are not provided") {
          Get(s"$url?startDate=2011-12-03&endDate=2011-12-05") ~> sealRoute(routes) ~> check {
            status shouldBe StatusCodes.Unauthorized
          }
        }
      }

      describe("POST should return") {
        describe("Created(201)") {
          it("when transaction is new") {
            (accountsDao.insertTransaction _).expects(txn1).returns(Source.single(Done("OK")))
            Post(
              url,
              txn1
            ) ~> addCredentials(credentials) ~> routes ~> check {
              status should be(StatusCodes.Created)
            }
          }
          it("when the same transaction is posted more than once") {
            def postAndCheck() {
              (accountsDao.insertTransaction _).expects(txn1).returns(Source.single(Done("OK")))
              Post(
                url,
                txn1
              ) ~> addCredentials(credentials) ~> routes ~> check {
                status should be(StatusCodes.Created)
              }
            }
            postAndCheck()
            postAndCheck()
          }
        }
        describe("BadRequest(400)") {
          describe("when date") {
            it("is in incorrect format") {
              def badDateRequest: String =
                """|{
                  |  "amount": 42.34,
                  |  "currency": "USD",
                  |  "date": "IllegalDate",
                  |  "message": "Random message"
                  |}
                """.
                  stripMargin
              Post(
                url,
                HttpEntity(MediaTypes.`application/json`, badDateRequest)
              ) ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
                status should be(StatusCodes.BadRequest)
                responseAs[String] should include("date")
              }
            }
            it("is not JSON string") {
              def badDateRequest: String =
                """|{
                  |  "amount": 42.34,
                  |  "currency": "USD",
                  |  "date": {},
                  |  "message": "Random message"
                  |}
                """.stripMargin
              Post(
                url,
                HttpEntity(MediaTypes.`application/json`, badDateRequest)
              ) ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
                status should be(StatusCodes.BadRequest)
                responseAs[String] should include("date")
              }
            }
          }
          describe("when currency") {
            it("is not able to be parsed") {
              def badCurrencyRequest: String =
                """|{
                  |  "amount": 42.34,
                  |  "currency": "InvalidCurrency",
                  |  "date": "2016-03-25T11:25:56.000000012",
                  |  "message": "Random message"
                  |}
                """.stripMargin
              Post(
                url,
                HttpEntity(MediaTypes.`application/json`, badCurrencyRequest)
              ) ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
                status should be(StatusCodes.BadRequest)
                responseAs[String] should (include("currency") and include("USD"))
              }
            }
            it("is not JSON string") {
              def badCurrencyRequest: String =
                """|{
                  |  "amount": 42.34,
                  |  "currency": {},
                  |  "date": "2016-03-25T11:25:56.000000012",
                  |  "message": "Random message"
                  |}
                """.stripMargin
              Post(
                url,
                HttpEntity(MediaTypes.`application/json`, badCurrencyRequest)
              ) ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
                status should be(StatusCodes.BadRequest)
                responseAs[String] should (include("currency") and include("USD"))
              }
            }
          }
        }
        it("InternalServerError(500) when DAO operation returns failed stream") {
          (accountsDao.insertTransaction _).expects(txn1).returns(Source.fromFuture(Future {
            throw new Exception("For test purpose")
          }))
          Post(
            url,
            txn1
          ) ~> addCredentials(credentials) ~> sealRoute(routes) ~> check {
            status should be(StatusCodes.InternalServerError)
          }
        }
        it("Unauthorized(401) when credentials are not provided") {
          Post(url, txn1) ~> sealRoute(routes) ~> check {
            status shouldBe StatusCodes.Unauthorized
          }
        }
      }
    }
  }
}
