package com.moneytracker.account

import java.time.LocalDateTime
import java.util.Currency

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.testkit.TestKit
import com.moneytracker.dto.Transaction
import com.moneytracker.specs.UnitSpec
import com.typesafe.config.ConfigFactory
import org.mongodb.scala.model.Filters
import org.mongodb.scala.{Document, MongoClient, MongoCollection}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

class MongodbAccountsDaoComponentSpec extends TestKit(ActorSystem("TestKitUsageSpec"))
  with UnitSpec with BeforeAndAfterEach with MongodbAccountsDaoComponent with ScalaFutures with BeforeAndAfterAll {

  override implicit def actorSystem: ActorSystem = system
  implicit val materializer = ActorMaterializer()

  private val mongoClient: MongoClient = MongoClient(ConfigFactory.load.getString("accounts.mongo.url"))
  private val db = mongoClient.getDatabase("account-service")

  private val transactions: MongoCollection[Document] = db.getCollection("transactions")

  private val dateTime1: LocalDateTime = LocalDateTime.of(2016, 1, 1, 15, 0, 0)
  private val dateTime2: LocalDateTime = LocalDateTime.of(2016, 1, 2, 15, 0, 0)
  private val dateTime3: LocalDateTime = LocalDateTime.of(2016, 1, 3, 15, 0, 0)
  private val dateTime4: LocalDateTime = LocalDateTime.of(2016, 1, 4, 15, 0, 0)
  val txn1 = Transaction(1, Currency.getInstance("USD"), dateTime1, Some("Message"))
  val txn2 = Transaction(2, Currency.getInstance("GBP"), dateTime2, Some("Another message"))
  val txn3 = Transaction(3, Currency.getInstance("EUR"), dateTime3, Some("One more message"))

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    clearTransactions()
  }

  override protected def afterAll(): Unit = {
    super.afterAll()
    clearTransactions()
    mongoClient.close()
    accountsDao.close()
  }

  def clearTransactions(): Unit = {
    Await.result(transactions.deleteMany(Filters.exists("_id")).head(), Duration.Inf)
  }

  describe("insertTransaction") {
    it("should store transaction in DB and it should exists in transactionWithin sequence") {
      accountsDao.insertTransaction(txn1).runWith(Sink.head).futureValue shouldBe Done("Credit transaction saved")
      accountsDao.transactionsWithin(dateTime1.toLocalDate, dateTime3.toLocalDate)
        .grouped(Int.MaxValue)
        .runWith(Sink.head)
        .futureValue shouldBe List(txn1)
    }

    it("should store transaction without message in DB and it should exists in transactionWithin sequence") {
      val txn: Transaction = txn1.copy(message = None)
      accountsDao.insertTransaction(txn).runWith(Sink.head).futureValue shouldBe Done("Credit transaction saved")
      accountsDao.transactionsWithin(dateTime1.toLocalDate, dateTime3.toLocalDate)
        .grouped(Int.MaxValue)
        .runWith(Sink.head)
        .futureValue shouldBe List(txn)
    }

    it("should store multiple transactions in DB and they should exist in transactionWithin sequence") {
      val done: Done = Done("Credit transaction saved")
      val insertFuture = for {
        res1 <- accountsDao.insertTransaction(txn1).runWith(Sink.head)
        res2 <- accountsDao.insertTransaction(txn2).runWith(Sink.head)
        res3 <- accountsDao.insertTransaction(txn3).runWith(Sink.head)
      } yield (res1, res2, res3)
      insertFuture.futureValue shouldBe(done, done, done)
      accountsDao.transactionsWithin(dateTime1.toLocalDate, dateTime4.toLocalDate)
        .grouped(Int.MaxValue)
        .runWith(Sink.head)
        .futureValue should contain theSameElementsInOrderAs List(txn1, txn2, txn3)
    }
  }

  it("transactionWithin should return only transactions within start and end dates") {
    val done: Done = Done("Credit transaction saved")
    val insertFuture = for {
      res1 <- accountsDao.insertTransaction(txn1).runWith(Sink.head)
      res2 <- accountsDao.insertTransaction(txn2).runWith(Sink.head)
      res3 <- accountsDao.insertTransaction(txn3).runWith(Sink.head)
    } yield (res1, res2, res3)
    insertFuture.futureValue shouldBe(done, done, done)

    accountsDao.transactionsWithin(dateTime1.toLocalDate, dateTime3.toLocalDate)
      .grouped(Int.MaxValue)
      .runWith(Sink.head)
      .futureValue should contain theSameElementsInOrderAs List(txn1, txn2)
  }
}
