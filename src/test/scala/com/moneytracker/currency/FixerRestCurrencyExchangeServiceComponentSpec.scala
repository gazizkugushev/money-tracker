package com.moneytracker.currency

import java.time.LocalDate
import java.util.Currency

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.testkit.TestKit
import com.moneytracker.specs.UnitSpec
import spray.http.HttpRequest

import scala.concurrent.Future

class FixerRestCurrencyExchangeServiceComponentSpec extends TestKit(ActorSystem("TestKitUsageSpec"))
  with UnitSpec {
  implicit val materializer = ActorMaterializer()

  import system.dispatcher

  private val pipelineMock: (HttpRequest) => Future[FixerResponse] = mock[HttpRequest => Future[FixerResponse]]
  private val uutComponent = new FixerRestCurrencyExchangeServiceComponent {
    override implicit def actorSystem: ActorSystem = FixerRestCurrencyExchangeServiceComponentSpec.this.system
    override val currencyExchangeService: CurrencyExchangeService = new FixerRestCurrencyExchangeService(pipelineMock)
  }

  import uutComponent.{currencyExchangeService => uut}

  describe("exchangeRateByCurrencyCodes") {
    val usd: Currency = Currency.getInstance("USD")
    val eur: Currency = Currency.getInstance("EUR")
    val gbp: Currency = Currency.getInstance("GBP")
    it("should not invoke remote fixer REST method if value is already cached") {
      (pipelineMock.apply _).expects(*).once().returns(Future(FixerResponse(eur, LocalDate.now(), Map("USD" -> 42))))
      def invokeAndCheck() {
        uut.exchangeRateByCurrencyCodes(List(usd), eur)
          .runWith(Sink.head)
          .futureValue
          .keys should contain theSameElementsAs List("USD")
      }
      invokeAndCheck()
      invokeAndCheck()
    }
    it("should return exchange rate 1 from currency to itself without making remote call") {
      (pipelineMock.apply _).expects(*).never()
      uut.exchangeRateByCurrencyCodes(List(usd), usd)
        .runWith(Sink.head)
        .futureValue shouldBe Map("USD" -> 1)
    }
    it("should return failed stream when fixer returns less exchange rates than it was asked") {
      (pipelineMock.apply _).expects(*).returns(Future(FixerResponse(eur, LocalDate.now(), Map("USD" -> 42))))
      val failedFuture = uut.exchangeRateByCurrencyCodes(List(usd, eur), gbp)
        .runWith(Sink.head)
      whenReady(failedFuture.failed) { e =>
        e shouldBe an[IllegalArgumentException]
        e.getMessage should include("EUR")
      }
    }
  }
}