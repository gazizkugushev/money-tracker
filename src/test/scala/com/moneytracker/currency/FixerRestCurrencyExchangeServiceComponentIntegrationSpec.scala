package com.moneytracker.currency

import java.util.Currency

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.testkit.TestKit
import com.moneytracker.specs.UnitSpec
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Second, Seconds, Span}

class FixerRestCurrencyExchangeServiceComponentIntegrationSpec extends TestKit(ActorSystem("TestKitUsageSpec"))
  with UnitSpec with FixerRestCurrencyExchangeServiceComponent with ScalaFutures {
  override implicit val actorSystem: ActorSystem = system

  implicit val materializer = ActorMaterializer()
  private val patience: PatienceConfig = PatienceConfig(Span(5, Seconds), Span(1, Second))
  describe("exchangeRateByCurrencyCodes") {
    it("should return map with single entry when single from currency is provided") {
      currencyExchangeService.exchangeRateByCurrencyCodes(List(Currency.getInstance("USD")), Currency.getInstance("EUR"))
        .runWith(Sink.head).futureValue(patience).keys should contain theSameElementsAs List("USD")
    }
    it("should return map with 3 exchange entries when 3 exchange entries are provided") {
      currencyExchangeService.exchangeRateByCurrencyCodes(
        List(
          Currency.getInstance("USD"),
          Currency.getInstance("CAD"),
          Currency.getInstance("GBP")
        ),
        Currency.getInstance("AUD")
      ).runWith(Sink.head).futureValue(patience).keys should contain theSameElementsAs List("USD", "CAD", "GBP")
    }
  }
}