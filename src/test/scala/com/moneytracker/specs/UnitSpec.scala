package com.moneytracker.specs

import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{FunSpecLike, Matchers, OneInstancePerTest}

trait UnitSpec extends FunSpecLike with Matchers with MockFactory with OneInstancePerTest with ScalaFutures
