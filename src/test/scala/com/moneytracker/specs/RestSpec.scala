package com.moneytracker.specs

import org.scalamock.scalatest.MockFactory
import org.scalatest.{FunSpec, Matchers, OneInstancePerTest}
import spray.testkit.ScalatestRouteTest

class RestSpec extends FunSpec with Matchers with ScalatestRouteTest with MockFactory with OneInstancePerTest
